
var express = require('express');
var router = express.Router();
var usuarioModel = require('../usersModel/userModel');
var moment = require('moment')

/* registra un usuario 
 tener en cuenta que debe ingresar el formato- "birthday": "1980-05-10T03:00:00.000Z"*/
router.post('/usuariosModel', function (req, res) {

    console.log('post con usuarioModel');
    var usuario = new usuarioModel(req.body);
    usuario.save(function (err, respuesta) {
        if (err) {
            res.send(err)
        }
        res.send(respuesta);
    })

})
router.get('/usuarios', function (req, res) {

    console.log('get en usuario model');
    usuarioModel.find(function (err, usuarios) {
        if (err) {
            res.send(err)
        }
        res.send(usuarios);
    })

})

/* obtener usuarios por id */

router.get('/usuariosModel/:id', function (req, res) {
    console.log(req.params);
    console.log(req.headers);
    usuarioModel.findOne({ _id: req.params.id }, function (err, user) {
        if (err) {
            res.send(err)
        }
        res.send(user);
    })

})


/* busca x nombre o apellido, en el cuerpo debo mandar la cadena a buscar  {"name": "ane"} */

router.post('/search', function (req, res) {
    let user = [];
    console.log('get en usuario model');
    usuarioModel.find(function (err, usuarios) {
        if (err) {
            res.send(err)
        }

        for (let index = 0; index < usuarios.length; index++) {
            if ((usuarios[index].name.indexOf(req.body.name) !== -1) || (usuarios[index].lastName.indexOf(req.body.name) !== -1)) {
                console.log("encontro");
                user.push(usuarios[index])

            }

        }

        res.send(user);
    })

})




/* elimina un usuario */
router.delete('/usuariosModel/:id', function (req, res) {
    console.log(req.params);
    usuarioModel.remove({ _id: req.params.id }, function (err, user) {
        if (err) {
            res.send(err)
        }
        res.json({ msg: 'usuario elminado' });
    })

})
/* modifica un usuario */
router.put('/usuariosModel/:id', function (req, res) {
    console.log(req.params);
    usuarioModel.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true }, function (err, user) {
        if (err) {
            res.send(err)
        }
        res.send(user);
    })

})

/* funcion que devuelve los usuarios que cumplen entre dos fecha que ingresa el cliente, las Fechas se deben 
ingresar por POSTMAN en formato  {"fechainicio": "1980-10-04","fechafin": "1987-10-04"} , y me devuelve los
 usuarios que cumplen años entre las fechas ingresadas*/
router.post('/usuarios/entreFechas', function (req, res) {
    let user = [];
    usuarioModel.find(function (err, usuarios) {
        if (err) {
            res.send(err)
        }

        for (let index = 0; index < usuarios.length; index++) {
            let fecha = moment(usuarios[index].birthday).format('YYYY-MM-DD')

            fecha = "'" + fecha + "'"
            if (moment(fecha).isBetween("'" + req.body.fechainicio + "'", "'" + req.body.fechafin + "'")) {
                user.push(usuarios[index]);
            };

        }
        res.send(user);

    })

})


/* api que debe consumir para filtrar los usuarios que tengan fecha de cumpleaños despues de la fecha 
ingresada por el cliente */

router.post('/usuarios/despuesFechas', function (req, res) {
    let user = [];
    usuarioModel.find(function (err, usuarios) {
        if (err) {
            res.send(err)
        }
        for (let index = 0; index < usuarios.length; index++) {
            let fecha = moment(usuarios[index].birthday).format('YYYY-MM-DD')
            fecha = "'" + fecha + "'"
            if (moment(fecha).isAfter("'" + req.body.fechainicio + "'")) {
                user.push(usuarios[index]);
            };

        }
        res.send(user);

    })

})

/* api que debe consumir para filtrar los usuarios que tengan fecha de cumpleaños antes de la fecha 
ingresada por el cliente */
router.post('/usuarios/antesFechas', function (req, res) {
    let user = [];
    usuarioModel.find(function (err, usuarios) {

        if (err) {
            res.send(err)
        }
        // moment('2018-08-11').isAfter('2018-08-10');
        for (let index = 0; index < usuarios.length; index++) {
            console.log(usuarios.length);
            console.log(usuarios[index].birthday);
            let fecha = moment(usuarios[index].birthday).format('YYYY-MM-DD')
            console.log("formato", "'" + fecha + "'");
            fecha = "'" + fecha + "'"
            if (moment(fecha).isBefore("'" + req.body.fechainicio + "'")) {
                user.push(usuarios[index]);
            };

        }
        res.send(user);

    })

})





module.exports = router;