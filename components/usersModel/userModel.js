
const mongoose = require('mongoose');
/* mongoose.set('useCreateIndex', true); */

const userSchema = mongoose.Schema({
  name: {
    type: String,

  },
  lastName: {
    type: String,

  },
  birthday: {
    type: Date,


  },
  dni: {
    type: String,

  }


});

const usuario = mongoose.model('users', userSchema);
module.exports = usuario;